(defproject minerva-clojure "0.1.0-SNAPSHOT"
  :description "This is a simple program intended to demonstrate a solution to tic-tac-toe."
  :url "https://github.com/dylan-ashley/minerva-clojure"
  :license {:name "The MIT License"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.8.0"]]
  :main ^:skip-aot minerva-clojure.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
